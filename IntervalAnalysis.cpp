#include <cstdio>
#include <iostream>
#include <set>
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/SourceMgr.h"
#include <llvm/IR/Constants.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

using namespace llvm;

/* Denotes the interval of a variable */
struct Interval {
	long min;
	long max;
	bool Null;
};


/* Data Structure for a node in the expression curve */
struct expressionNode{
        Instruction* instPtr;
	bool isConstant;
        bool operator_or_variable; //true if it is a binary operator, else, it is an variable.
        std::string variable_name; //Contain a valid variable name(x,y,z) if variable, else will contain the name of the operator.
        expressionNode* left;
	expressionNode* right;      
};



/* Data Structure for storing the compare instruction*/
struct compareInstructionInfo{
	Interval* trueInterval;
	Interval* falseInterval;
};

/* Structure to store the X, Y  after the result of a backward calculation */
struct XYPair{
	Interval* X;
	Interval* Y;
};

/* This struct is tailored towards avoiding paths which are not necessary */
struct IntervalMapOrEmpty{
	std::map<Instruction*, Interval*> intervalMap;
	bool empty;
};

void generateCFG(BasicBlock*,std::map<Instruction*, Interval*>);
std::map<Instruction*, Interval*> getNewupdatedMap(BasicBlock*,std::map<Instruction*,Interval*>);
std::map<Instruction*, Interval*> createNewMap(std::map<Instruction*, Interval*>);
Interval* getNewInterval(long, long, bool, bool);
Interval* getIntervalForExpression(std::string, Interval*, Interval*);
Interval* OpAdd(Interval*, Interval*);
Interval* OpSub(Interval*, Interval*);
Interval* OpMul(Interval*, Interval*);
Interval* OpUDiv(Interval*, Interval*);
Interval* OpSDiv(Interval*, Interval*);
long div(long, long);
long mul(long, long);
long sub(long, long);
long add(long, long);
Interval* intervalUnion(Interval*, Interval*);
Interval* intervalIntersection(Interval*, Interval*);
bool isBoundedByMinusInfinity(long,long);
bool isBoundedByInfinity(long,long);
void outputAbstractedIntervals(std::map<Instruction*, Interval*>);
std::map<Instruction*, Interval*> mergeMaps(std::map<Instruction*, Interval*>, std::map<Instruction*, Interval*>);
compareInstructionInfo* solveCompareInstruction(Instruction* , llvm::CmpInst::Predicate, Interval*, Interval*, std::map<Instruction*, Interval*>);
void printIntervals(Interval*, Interval*);
expressionNode* createExpressionGraph(Instruction* labelInst, Value* op1, std::string opcode, Value* op2);
void outputExpressionGraph(expressionNode*);
IntervalMapOrEmpty* getIntervalMapForBlock(Instruction*, std::map<Instruction*, Interval*>, bool);
std::map<Instruction*, Interval*> backtrackOnGraph(expressionNode*, Interval*, std::map<Instruction*, Interval*>);
XYPair* updateXAndY(Interval*, Interval*, Interval*, std::string);
XYPair* backwardADD(Interval*, Interval*, Interval*);
XYPair* backwardSUB(Interval*, Interval*, Interval*);
XYPair* backwardMUL(Interval*, Interval*, Interval*);
XYPair* backwardDIV(Interval*, Interval*, Interval*);
void  outputXYPair(XYPair*);
bool isIntervalNull(Interval*);
std::set<BasicBlock*> visitedNode;
bool checkIfFixedPointIsReached(BasicBlock*, std::map<Instruction*, Interval*>);
void getFixedPointOfLoop(BasicBlock*, std::map<Instruction*, Interval*>);
bool isSameInterval(Interval*, Interval*);
void detectAllloopBlocks(BasicBlock*, std::set<BasicBlock*>);
bool anyChangeinRangeofValues(BasicBlock*, std::map<Instruction*, Interval*>);
void performWidening(BasicBlock*, std::map<Instruction*, Interval*>);
Interval* widen(Interval*, Interval*, long);

std::map<Instruction*, Interval*> lastBlockInterval;
bool isLastBlockSet = false;
std::map<Instruction*, long> allInstructionList;
std::map<Instruction*, expressionNode*> instructionGraph;
std::map<Instruction*, compareInstructionInfo*> compareInstGraph;
std::map<BasicBlock*, IntervalMapOrEmpty*> bbToSpecificIntervalMap; 
/* Stores the pointer to the loop blocks*/
std::set<BasicBlock*> loopBlocks;
std::map<BasicBlock*, std::map<Instruction*, Interval*>> WideningToIntervalMap;
std::map<BasicBlock*, long> bbToLoopUnrollCounter;

//long count = 0;
bool inLoop = false;
long MAX_LOOPS = 1000; /* Maximum loop unrolling is 1000 */

int main(int argc, char **argv)
{
    // Read the IR file.
    LLVMContext &Context = getGlobalContext();
    SMDiagnostic Err;
    Module *M = ParseIRFile(argv[1], Err, Context);
    if (M == nullptr)
    {
      fprintf(stderr, "error: failed to load LLVM IR file \"%s\"", argv[1]);
      return EXIT_FAILURE;
    }

    std::map<Instruction*, Interval*> bbToIntervalMap; 
    std::set<BasicBlock*> visitedNode;
 
    for (auto &F: *M)
      if (strncmp(F.getName().str().c_str(),"main",4) == 0){
	  BasicBlock* BB = dyn_cast<BasicBlock>(F.begin());
	  /* detects all the loop blocks and updates the loop blocks. */
          detectAllloopBlocks(BB, visitedNode);
	  generateCFG(BB,bbToIntervalMap);
      }
    outputAbstractedIntervals(lastBlockInterval);

    return 0;
}


void generateCFG(BasicBlock* BB,std::map<Instruction*, Interval*> bbToIntervalMap)
{
  if (loopBlocks.find(BB) != loopBlocks.end()) {
		/* A loop is detected and If there is a change in newSecretVars, then pass else Not */

		//isLoopContext = true;
		//resetLoopInfo();
		if (bbToLoopUnrollCounter.find(BB) == bbToLoopUnrollCounter.end())
			bbToLoopUnrollCounter.insert(std::make_pair(BB, 0));
		getFixedPointOfLoop(BB, createNewMap(bbToIntervalMap));
		//std::map<Instruction*, Interval*> newUpdatedMap = WideningToIntervalMap.find(BB)->second;
		//markUnboundedVariablesAfterLoop(BB);
		//resetLoopInfo();
		//isLoopContext = false;

		const TerminatorInst *TInst = BB->getTerminator();
		BasicBlock *Succ = TInst->getSuccessor(1);
		//std::map<Instruction*, Interval*> bbToIntervalMapNew = createNewMap(newUpdatedMap);
		if (bbToLoopUnrollCounter.find(BB)->second > 1){
			std::map<Instruction*, Interval*> newUpdatedMap = WideningToIntervalMap.find(BB)->second;
			std::map<Instruction*, Interval*> bbToIntervalMapNew = createNewMap(newUpdatedMap);
			generateCFG(Succ, bbToIntervalMapNew);
		}
		else{
			bbToLoopUnrollCounter[BB] = 0;
			generateCFG(Succ, bbToIntervalMap);
		}
  } else {
  	std::map<Instruction*, Interval*> newUpdatedMap = getNewupdatedMap(BB,bbToIntervalMap);

  	// Pass secretVars list to child BBs and check them
  	const TerminatorInst *TInst = BB->getTerminator();
  	int NSucc = TInst->getNumSuccessors();
  	for (int i = 0;  i < NSucc; ++i) {
    	BasicBlock *Succ = TInst->getSuccessor(i);
    	if (bbToSpecificIntervalMap.find(Succ) ==  bbToSpecificIntervalMap.end()){
    		std::map<Instruction*, Interval*> bbToIntervalMapNew = createNewMap(newUpdatedMap);
		generateCFG(Succ, bbToIntervalMapNew);
	} else
		if (!bbToSpecificIntervalMap.find(Succ)->second->empty)
			generateCFG(Succ, bbToSpecificIntervalMap.find(Succ)->second->intervalMap);
  	}

  	// Last Basic Block, check if secret leaks to public
  	if (NSucc == 0){
		//Need to merge the interval maps when you reach the last part.
		if (!isLastBlockSet){
			lastBlockInterval = createNewMap(newUpdatedMap);
			isLastBlockSet = true;
		}
		else
			lastBlockInterval = mergeMaps(newUpdatedMap, lastBlockInterval);
  		}
  	}	
}


/* While loop related functions */
void getFixedPointOfLoop(BasicBlock* BB, std::map<Instruction*, Interval*> bbToIntervalMap){
	/* This function gets the fixed point of the block */
	if (loopBlocks.find(BB) != loopBlocks.end()){
		bool isReached = checkIfFixedPointIsReached(BB, bbToIntervalMap);
		if (isReached){
		} else {
			performWidening(BB, createNewMap(bbToIntervalMap));
		//	inLoop = true;
			std::map<Instruction*, Interval*> newUpdatedMap = getNewupdatedMap(BB,bbToIntervalMap);
		//	inLoop = false;
			long count = bbToLoopUnrollCounter.find(BB)->second;
			bbToLoopUnrollCounter[BB] = count + 1;
			/* Only use the loop block BasicBlock and not the terminating one */
			const TerminatorInst *TInst = BB->getTerminator();
			BasicBlock *Succ = TInst->getSuccessor(0);
			std::map<Instruction*, Interval*> bbToIntervalMapNew = createNewMap(newUpdatedMap);
			if (bbToSpecificIntervalMap.find(Succ) !=  bbToSpecificIntervalMap.end())
			{
				if (!bbToSpecificIntervalMap.find(Succ)->second->empty) {
					getFixedPointOfLoop(Succ, bbToIntervalMapNew);
				}
			}
		}
	} else {
		/* Not a loop block, hence calculate the */
		const TerminatorInst *TInst = BB->getTerminator();
		std::map<Instruction*, Interval*> updatedMap = getNewupdatedMap(BB, bbToIntervalMap);
                int NSucc = TInst->getNumSuccessors();
		for (int i=0; i<NSucc; ++i){
			BasicBlock *Succ = TInst->getSuccessor(i);
			if (bbToSpecificIntervalMap.find(Succ) ==  bbToSpecificIntervalMap.end()){
                		std::map<Instruction*, Interval*> bbToIntervalMapNew = createNewMap(updatedMap);
		                generateCFG(Succ, bbToIntervalMapNew);
        		} else {
                		if (!bbToSpecificIntervalMap.find(Succ)->second->empty)
                        		generateCFG(Succ, bbToSpecificIntervalMap.find(Succ)->second->intervalMap);
        		}

		}
	}
	
}

void performWidening(BasicBlock* BB, std::map<Instruction*, Interval*> bbToIntervalMap){
	long count  = bbToLoopUnrollCounter.find(BB)->second;
	if (WideningToIntervalMap.find(BB) != WideningToIntervalMap.end()){
		std::map<Instruction*, Interval*> oldMap = WideningToIntervalMap.find(BB)->second;
		std::map<Instruction*, Interval*> newMap;
                for(auto it = bbToIntervalMap.cbegin(); it != bbToIntervalMap.cend(); ++it ){
                        Interval* newInt = it->second;
                        Interval* oldInt = oldMap.find(it->first)->second;
			Interval* widenedInterval = widen(oldInt, newInt, count);
			newMap.insert(std::make_pair(it->first, widenedInterval));
                }
		WideningToIntervalMap[BB] = newMap;
	} else {
		if (count > 0)
			WideningToIntervalMap.insert(std::make_pair(BB, createNewMap(bbToIntervalMap)));
	}
}

Interval* widen(Interval* X, Interval* Y, long count){
	if (isIntervalNull(X))
		return Y;
	if (isIntervalNull(Y))
		return X;
	long a,b,c,d;
	a = X->min;
	b = X->max;
	c = Y->min;
	d = Y->max;
	long min, max;
	if (count <= MAX_LOOPS){
		min = std::min(a,c);
		max = std::max(b,d);
		return getNewInterval(min, max, false, false);
	}
	if (a<=c)
		min = a;
	else
		min = std::numeric_limits<long>::min();

	if (b>=d)
		max = b;
	else
		max = std::numeric_limits<long>::max();

	return getNewInterval(min, max, false, false);
}

void detectAllloopBlocks(BasicBlock* BB, 
			 std::set<BasicBlock*> visitedNode){
        //printVisitedBlocks(visitedNode);
        if (visitedNode.find(BB)  == visitedNode.end()){
		visitedNode.insert(BB);
                const TerminatorInst *TInst = BB->getTerminator();
        	int NSucc = TInst->getNumSuccessors();
		for (int i = 0;  i < NSucc; ++i) {
			std::set<BasicBlock*> visitedNodeToChild = visitedNode;
               		BasicBlock *Succ = TInst->getSuccessor(i);
                        detectAllloopBlocks(Succ, visitedNodeToChild);
		}
	}
	else{
		/* Found a loop, hence add it.*/
                if (loopBlocks.find(BB) == loopBlocks.end()){
			loopBlocks.insert(BB);
		}

	}
	return;
      
}

/* -------------------------------- End of while related function ---------------------   */


std::map<Instruction*, Interval*> getNewupdatedMap(BasicBlock* BB,
				                   std::map<Instruction*, Interval*> bbToIntervalMap)
{
	std::map<Instruction*, Interval*> bbToIntervalMapNew = createNewMap(bbToIntervalMap);
	
	/*Function which checks Leakage of public to private variables*/
        for (auto &I: *BB){
		allInstructionList.insert(std::make_pair(dyn_cast<Instruction>(&I), 0));
                /* Check for allocate Instruction */
                if (isa<AllocaInst>(I)){
       		         /* Handle allocaInstruction */
                         Instruction* variable = dyn_cast<Instruction>(&I);
			 Interval* interval = getNewInterval(-1,-1, true, false);
			 if (bbToIntervalMapNew.find(variable) == bbToIntervalMapNew.end())
			 	bbToIntervalMapNew.insert(std::make_pair(variable, interval));
			 else
				bbToIntervalMapNew[variable] = interval;
                         
		}
                else if (isa<LoadInst>(I)){
                        Instruction* labelInst = dyn_cast<Instruction>(&I);
			Value* op1_val = I.getOperand(0);
                        Instruction* op1 = dyn_cast<Instruction>(I.getOperand(0));
                        //printf("%s\n", op1->getName().str().c_str());
			Interval* interval = new Interval;
			if (bbToIntervalMapNew.find(op1) == bbToIntervalMapNew.end()){
				printf("Error encountered");
			} else {
				Interval* assignmentInterval = bbToIntervalMapNew.find(op1)->second;
				interval->min = assignmentInterval->min;
				interval->max = assignmentInterval->max;
				interval->Null = false;
				if (bbToIntervalMapNew.find(labelInst) == bbToIntervalMapNew.end())
					bbToIntervalMapNew.insert(std::make_pair(labelInst, interval));
				else
					bbToIntervalMapNew[labelInst] = interval;
			}
			expressionNode* root = createExpressionGraph(labelInst, op1_val, "load", NULL);
			if (instructionGraph.find(labelInst) == instructionGraph.end())
				instructionGraph.insert(std::make_pair(labelInst, root));
			else
				instructionGraph[labelInst] = root;
                	//outputExpressionGraph(root);
		}
                else if (isa<StoreInst>(I)){
			/* Found a store Instruction */
                        Value* v = I.getOperand(0);
      			Instruction* op1 = dyn_cast<Instruction>(v);
                        Instruction* op2 = dyn_cast<Instruction>(I.getOperand(1));
			Interval* interval;
                        if (ConstantInt* CI = dyn_cast<ConstantInt>(v)){
                                long val = CI->getSExtValue();
				interval = getNewInterval(val,val, false, false);
			} else{
			        Instruction* op1 = dyn_cast<Instruction>(v);
				Interval* assignmentInterval = bbToIntervalMapNew.find(op1)->second;
				interval = getNewInterval(assignmentInterval->min, assignmentInterval->max, false, false);
			}
			
			if (bbToIntervalMapNew.find(op2) == bbToIntervalMapNew.end()){
				bbToIntervalMapNew.insert(std::make_pair(op2, interval));
			} else {
				bbToIntervalMapNew[op2] = interval;
			}			

		}
                else if (isa<CmpInst>(I)){
			if (inLoop){
				return bbToIntervalMapNew;
			}
                        /* Do nothing in this case */
                        Instruction* labelInst = dyn_cast<Instruction>(&I);
			std::string opcode = I.getOpcodeName();
			Value* op1 = I.getOperand(0);
			Value* op2 = I.getOperand(1);
			Interval* interval1;
			Interval* interval2;
			
			if (ConstantInt* CI = dyn_cast<ConstantInt>(op1)){
				long val = CI->getSExtValue();
				interval1 = getNewInterval(val, val, false, false);	
			} else {
				Instruction* insOp1 = dyn_cast<Instruction>(op1);
				Interval* assignmentInterval = bbToIntervalMapNew.find(insOp1)->second;
				interval1 = getNewInterval(assignmentInterval->min, assignmentInterval->max, false, false);
			}

			if (ConstantInt* CI = dyn_cast<ConstantInt>(op2)){
				long val = CI->getSExtValue();
                                interval2 = getNewInterval(val, val, false, false); 
                        } else {
				Instruction* insOp1 = dyn_cast<Instruction>(op2);
                                Interval* assignmentInterval = bbToIntervalMapNew.find(insOp1)->second;
                                interval2 = getNewInterval(assignmentInterval->min, assignmentInterval->max, false, false);
                        }
			expressionNode* root = createExpressionGraph(labelInst, op1, "sub", op2);
			if (instructionGraph.find(labelInst) == instructionGraph.end())
				 instructionGraph.insert(std::make_pair(labelInst, root));
			else
				 instructionGraph[labelInst] = root;
			CmpInst *cmpInst = dyn_cast<CmpInst>(&I);
			compareInstructionInfo* cmpInstInfo = solveCompareInstruction(dyn_cast<Instruction>(&I) , cmpInst->getPredicate(), interval1, interval2, bbToIntervalMapNew);
			if (compareInstGraph.find(dyn_cast<Instruction>(&I)) == compareInstGraph.end())
				compareInstGraph.insert(std::make_pair(dyn_cast<Instruction>(&I), cmpInstInfo));
			else
				compareInstGraph[dyn_cast<Instruction>(&I)] = cmpInstInfo;
		}
                else if (isa<BranchInst>(I)){
			if (inLoop){
                                return bbToIntervalMapNew;
                        }

			/* Do nothing in this case */
			BranchInst* labelInst = dyn_cast<BranchInst>(&I);
			Value* condition = I.getOperand(0);
			Instruction* inst = dyn_cast<Instruction>(condition);
			if (compareInstGraph.find(inst) != compareInstGraph.end() && labelInst->isConditional()){
				BasicBlock* truthBlock = labelInst->getSuccessor(0);
				BasicBlock* falseBlock = labelInst->getSuccessor(1);
				std::map<Instruction*, Interval*> truthBlockMap = createNewMap(bbToIntervalMapNew);
				std::map<Instruction*, Interval*> falseBlockMap = createNewMap(bbToIntervalMapNew);
				IntervalMapOrEmpty* truthBlockMapOrEmpty = getIntervalMapForBlock(inst, truthBlockMap, true);
				IntervalMapOrEmpty* falseBlockMapOrEmpty = getIntervalMapForBlock(inst, falseBlockMap, false);
				if (bbToSpecificIntervalMap.find(truthBlock) == bbToSpecificIntervalMap.end())
					bbToSpecificIntervalMap.insert(std::make_pair(truthBlock, truthBlockMapOrEmpty));
				else
					bbToSpecificIntervalMap[truthBlock] = truthBlockMapOrEmpty;

				if (bbToSpecificIntervalMap.find(falseBlock) == bbToSpecificIntervalMap.end())
                                        bbToSpecificIntervalMap.insert(std::make_pair(falseBlock, falseBlockMapOrEmpty));
                                else
                                        bbToSpecificIntervalMap[falseBlock] = falseBlockMapOrEmpty;

			} else {
			}
			
                }
                else if (isa<BinaryOperator>(I)){
                        std::string opcode = I.getOpcodeName();
			Instruction* labelInst = dyn_cast<Instruction>(&I);
			
			Value* op1 = I.getOperand(0);
			Value* op2 = I.getOperand(1);
			long val1, val2;
                        Interval* interval1;
			Interval* interval2;
			if (ConstantInt* CI = dyn_cast<ConstantInt>(op1)){
				val1 = CI->getSExtValue();
				interval1 = getNewInterval(val1,val1, false, false);
		 	} else {
				Instruction* insOp1 = dyn_cast<Instruction>(op1);
				Interval* assignmentInterval = bbToIntervalMapNew.find(insOp1)->second;
				interval1 = getNewInterval(assignmentInterval->min, assignmentInterval->max, false, false);
			}
			if (ConstantInt* CI = dyn_cast<ConstantInt>(op2)){
                                val2 = CI->getSExtValue();
				interval2 = getNewInterval(val2,val2, false, false);
                        } else {
				Instruction* insOp2 = dyn_cast<Instruction>(op2);
			        Interval* assignmentInterval = bbToIntervalMapNew.find(insOp2)->second;
                                interval2 = getNewInterval(assignmentInterval->min, assignmentInterval->max, false, false);
                        }
			expressionNode* root = createExpressionGraph(labelInst, op1, opcode, op2);
			if (instructionGraph.find(labelInst) == instructionGraph.end())
                        	instructionGraph.insert(std::make_pair(labelInst, root));
			else
				instructionGraph[labelInst] = root;

			Interval* resultInterval = getIntervalForExpression(opcode, interval1, interval2);
                        if (bbToIntervalMapNew.find(labelInst) == bbToIntervalMapNew.end())
				bbToIntervalMapNew.insert(std::make_pair(labelInst, resultInterval));
			else
				bbToIntervalMapNew[labelInst] = resultInterval;
                }
		else{
			/* Other Instruction */
		}
        }
	return bbToIntervalMapNew;
  
}

expressionNode* createExpressionGraph(Instruction* labelInst, Value* op1, std::string opcode, Value* op2){
	/* This function creates the expression graph of an expression */
	expressionNode* node = new expressionNode;
	node->operator_or_variable = true;
	if (opcode.compare("load") == 0)
		node->variable_name = "add";
	else
		 node->variable_name = opcode;
	node->isConstant = false;
	node->instPtr = labelInst;
	if (ConstantInt* CI = dyn_cast<ConstantInt>(op1)){
        	long val = CI->getSExtValue();
		expressionNode* child = new expressionNode;
		child->operator_or_variable = false; 
		child->variable_name = std::to_string(val);
		child->isConstant = true;
		child->instPtr = NULL;
		child->left = NULL;
		child->right = NULL;
		node->left = child; 
	} else {
		Instruction* op = dyn_cast<Instruction>(op1);
		if (op->hasName()){
			expressionNode* child = new expressionNode;
			child->operator_or_variable = false;
			child->variable_name = op->getName().str();
			child->isConstant = false;
			child->instPtr = op;
			child->left = NULL;
			child->right = NULL;
			node->left = child;
		} else {
			node->left = instructionGraph.find(op)->second;
		}
	}
	if (opcode.compare("load") == 0){
                node->variable_name = "add";
                expressionNode* child = new expressionNode;
                child->operator_or_variable = false;
                child->variable_name = std::to_string(0);
                child->isConstant = true;
                child->instPtr = NULL;
		child->left = NULL;
                child->right = NULL;
                node->right = child;
        } else if (ConstantInt* CI = dyn_cast<ConstantInt>(op2)){
                long val = CI->getSExtValue();
                expressionNode* child = new expressionNode;
                child->operator_or_variable = true;
		child->variable_name = std::to_string(val);
                child->isConstant = true;
                child->instPtr = NULL;
		child->left = NULL;
                child->right = NULL;
                node->right = child;
        } else {
                Instruction* op = dyn_cast<Instruction>(op2);
                if (op2->hasName()){
                        expressionNode* child = new expressionNode;
                        child->operator_or_variable = false;
			child->variable_name = op->getName().str();
                        child->isConstant = false;
                        child->instPtr = op;
			child->left = NULL;
                        child->right = NULL;
                        node->right = child;
                } else {
                        node->right = instructionGraph.find(op)->second;
                }
        }
	//outputExpressionGraph(node);
	return node;
}

IntervalMapOrEmpty* getIntervalMapForBlock(Instruction* condition, std::map<Instruction*, Interval*> bbToIntervalMapNew, bool flag){
	/* This function create the interval Map for a Block, by using the condition of a if-else statement */
	Interval* interval;
	if (flag){
		interval = compareInstGraph.find(condition)->second->trueInterval;
	} else {
		interval = compareInstGraph.find(condition)->second->falseInterval;
	}
	
	expressionNode* expNode = instructionGraph.find(condition)->second;
	IntervalMapOrEmpty* intervalMapOrEmpty = new IntervalMapOrEmpty;
	if (!isIntervalNull(interval)){
		intervalMapOrEmpty->empty = false;
		intervalMapOrEmpty->intervalMap = backtrackOnGraph(expNode, interval, bbToIntervalMapNew);
	} else
		intervalMapOrEmpty->empty = true;
	return intervalMapOrEmpty;
}

std::map<Instruction*, Interval*> backtrackOnGraph(expressionNode* expNode, Interval* interval, std::map<Instruction*, Interval*> bbToIntervalMapNew){
	/* This function conducts the backtracking on the expression graph and reintializes any required variable interval */
	if(expNode){
		if (expNode->operator_or_variable){
			expressionNode* left = expNode->left;
			expressionNode* right = expNode->right;
			Interval* X;
			Interval* Y;
			
			if (!left->isConstant){
				Instruction* inst = left->instPtr;
				X = bbToIntervalMapNew.find(inst)->second;
				
			} else {
				X = getNewInterval(std::stoi(left->variable_name), std::stoi(left->variable_name), false, false);
			}
			
			if (!right->isConstant){
				Instruction* inst = left->instPtr;
                                Y = bbToIntervalMapNew.find(inst)->second;
			} else {
				Y = getNewInterval(std::stoi(right->variable_name), std::stoi(right->variable_name), false, false);

			}
			

			XYPair* newPair = updateXAndY(X, Y, interval, expNode->variable_name);

			outputXYPair(newPair);
			/*
				In this step check if leaf is reached, then updatd the bbToIntervalMapNew.
			*/
			if (!left->isConstant){
				Instruction* inst = left->instPtr;
				bbToIntervalMapNew[inst] = newPair->X;
				if (left->operator_or_variable){
					bbToIntervalMapNew = backtrackOnGraph(left, newPair->X, bbToIntervalMapNew);
				} else {
				}
			}
			if (!right->isConstant){
                                Instruction* inst = right->instPtr;
                                bbToIntervalMapNew[inst] = newPair->Y;
				if (right->operator_or_variable){
                                } else {
				}
				
                        }
			
		}
	}
	return bbToIntervalMapNew;
}

bool isSameInterval(Interval* newInt, Interval* oldInt){
        if (newInt->Null && oldInt->Null)
		return true;
	else
		return (oldInt->min <= newInt->min) && (oldInt->max >= newInt->max);
}

bool anyChangeinRangeofValues(BasicBlock* BB, std::map<Instruction*, Interval*> bbToIntervalMap){
	if (WideningToIntervalMap.find(BB) == WideningToIntervalMap.end()){
		return true;
	} else {
		std::map<Instruction*, Interval*> oldMap = WideningToIntervalMap.find(BB)->second;
		for(auto it = bbToIntervalMap.cbegin(); it != bbToIntervalMap.cend(); ++it ){
                	Interval* newInt = it->second;
			Interval* oldInt = oldMap.find(it->first)->second;
			if (!isSameInterval(newInt, oldInt)){
				return true;	
			}
        	}
		return false;
		
	}
}


bool checkIfFixedPointIsReached(BasicBlock* BB, std::map<Instruction*, Interval*> bbToIntervalMap) {
	if (!anyChangeinRangeofValues(BB, bbToIntervalMap)){
		return true;
	}
	return false;
}


XYPair* backwardADD(Interval* X, Interval* Y, Interval* R){
	/* Does backward add  on the triplet (X,Y,R) */
	XYPair* pair = new XYPair;
	pair->X = intervalIntersection(X, OpSub(R, Y));
	pair->Y = intervalIntersection(Y, OpSub(R, X));
	return pair;
}

XYPair* backwardSUB(Interval* X, Interval* Y, Interval* R){
	/* Does backward sub  on the triplet (X,Y,R) */
	XYPair* pair = new XYPair;
        pair->X = intervalIntersection(X, OpAdd(R, Y));
        pair->Y = intervalIntersection(Y, OpSub(X, R));
        return pair;
}

XYPair* backwardDIV(Interval* X, Interval* Y, Interval* R){
	/* Does backward div on the triplet (X,Y,R) */
	XYPair* pair = new XYPair;
        pair->X = intervalIntersection(X, OpMul(R, Y));
        pair->Y = intervalIntersection(Y, intervalUnion(OpUDiv(R, X), getNewInterval(0,0,false,false)));
        return pair;
}

XYPair* backwardMUL(Interval* X, Interval* Y, Interval* R){
	/* Does backward mul on the triplet (X,Y,R) */
	XYPair* pair = new XYPair;
        pair->X = intervalIntersection(X, OpUDiv(R, Y));
        pair->Y = intervalIntersection(Y, OpUDiv(R, X));
        return pair;
}

XYPair* updateXAndY(Interval* X, Interval* Y, Interval* R, std::string opcode){
	/* Backward Analysis of intervals
	   Currently supported: (add, sub, div, mul) */
	if (opcode.compare("add") == 0){
		return backwardADD(X,Y,R);
	} else if (opcode.compare("sub") == 0){
		return backwardSUB(X,Y,R);
	} else if (opcode.compare("div") == 0){
		return backwardDIV(X,Y,R);
	} else if (opcode.compare("mul") == 0){
		return backwardMUL(X,Y,R);
	} else {
		return NULL;
	}
}

compareInstructionInfo* solveCompareInstruction(Instruction* I, llvm::CmpInst::Predicate predicate, Interval* i1, Interval* i2, std::map<Instruction*, Interval*> bbToIntervalMap){
	/* Solve for different equation and store the results to be usable by branch instructions */
	Interval* subtactedInterval = OpSub(i1, i2);
	Interval* trueInterval;
	Interval* falseInterval;
	if (predicate  == CmpInst::ICMP_SGT) {
		trueInterval = intervalIntersection(subtactedInterval, getNewInterval(1, std::numeric_limits<long>::max(), false, false));
		falseInterval = intervalIntersection(subtactedInterval, getNewInterval(std::numeric_limits<long>::min(), 0, false, false));
	} else if (predicate == CmpInst::ICMP_EQ) {
		trueInterval = intervalIntersection(subtactedInterval, getNewInterval(0,0, false, false));
		falseInterval = intervalUnion(intervalIntersection(subtactedInterval, getNewInterval(std::numeric_limits<long>::min(), -1, false, false)),
							intervalIntersection(subtactedInterval, getNewInterval(1, std::numeric_limits<long>::max(), false, false)));
	} else if (predicate == CmpInst::ICMP_NE) {
		trueInterval = intervalUnion(intervalIntersection(subtactedInterval, getNewInterval(std::numeric_limits<long>::min(), -1, false, false)),
                                                       intervalIntersection(subtactedInterval, getNewInterval(1, std::numeric_limits<long>::max(), false, false)));
		falseInterval = intervalIntersection(subtactedInterval, getNewInterval(0,0, false, false));
	
	} else if (predicate == CmpInst::ICMP_UGT) {
		trueInterval = intervalIntersection(subtactedInterval, getNewInterval(1, std::numeric_limits<long>::max(), false, false));
                falseInterval = intervalIntersection(subtactedInterval, getNewInterval(std::numeric_limits<long>::min(), 0, false, false));
	} else if (predicate == CmpInst::ICMP_UGE) {
		trueInterval = intervalIntersection(subtactedInterval, getNewInterval(0, std::numeric_limits<long>::max(), false, false));
                falseInterval = intervalIntersection(subtactedInterval, getNewInterval(std::numeric_limits<long>::min(), -1, false, false));
	} else if (predicate == CmpInst::ICMP_ULT) {
		trueInterval = intervalIntersection(subtactedInterval, getNewInterval(std::numeric_limits<long>::min(), -1, false, false));
                falseInterval = intervalIntersection(subtactedInterval, getNewInterval(0, std::numeric_limits<long>::max(), false, false));
	} else if (predicate == CmpInst::ICMP_ULE) {
		trueInterval = intervalIntersection(subtactedInterval, getNewInterval(std::numeric_limits<long>::min(), 0, false, false));
                falseInterval = intervalIntersection(subtactedInterval, getNewInterval(1, std::numeric_limits<long>::max(), false, false));
	} else if (predicate == CmpInst::ICMP_SGE) {
		trueInterval = intervalIntersection(subtactedInterval, getNewInterval(0, std::numeric_limits<long>::max(), false, false));
                falseInterval = intervalIntersection(subtactedInterval, getNewInterval(std::numeric_limits<long>::min(), -1, false, false));	
	} else if (predicate == CmpInst::ICMP_SLE) {
                trueInterval = intervalIntersection(subtactedInterval, getNewInterval(std::numeric_limits<long>::min(), 0, false, false));
                falseInterval = intervalIntersection(subtactedInterval, getNewInterval(1, std::numeric_limits<long>::max(), false, false));
        } else if (predicate == CmpInst::ICMP_SLT) {
                trueInterval = intervalIntersection(subtactedInterval, getNewInterval(std::numeric_limits<long>::min(), -1, false, false));
                falseInterval = intervalIntersection(subtactedInterval, getNewInterval(0, std::numeric_limits<long>::max(), false, false));
        } else {
		/* Unsupported predicate */
	}
	compareInstructionInfo* val = new compareInstructionInfo;
	val->trueInterval = trueInterval;
	val->falseInterval = falseInterval;
	return val;
	
}

Interval* OpMul(Interval* x, Interval* y){
	/* Interval Multiplication implementation */
	if (isIntervalNull(x) || isIntervalNull(y))
		return getNewInterval(-1, -1, false, true);
	long a, b, c, d;
        a = x->min;
	b = x->max;
	c = y->min;
	d = y->max;
        long min, max;
	if (isBoundedByMinusInfinity(a,c)){
		min = std::numeric_limits<long>::min();
	} else {
		min = std::min(std::min(std::min(mul(a,c), mul(a,d)), mul(b,c)), mul(b,d));
	}
  
        if (isBoundedByInfinity(b,d)){
		max = std::numeric_limits<long>::max();
	} else {
		max = std::max(std::max(std::max(mul(a,c), mul(a,d)), mul(b,c)), mul(b,d)); 
	}
	return getNewInterval(min, max, false, false);
}

Interval* OpAdd(Interval* x, Interval* y){
	/* Interval Addition implemention */
	if (isIntervalNull(x) || isIntervalNull(y))
                return getNewInterval(-1, -1, false, true);
        long a, b, c, d;
	a = x->min;
        b = x->max;
        c = y->min;
        d = y->max;
	long min, max;
	if (isBoundedByMinusInfinity(a,c)){
                min = std::numeric_limits<long>::min();
        } else{
		min = a + c;
	}

	if (isBoundedByInfinity(b,d)){
		max = std::numeric_limits<long>::max();
	} else {
		max = b + d;
	}
	return getNewInterval(min, max, false, false);
	
}

Interval* OpSub(Interval* x, Interval* y){
	/* Interval Substraction implementation */
	if (isIntervalNull(x) || isIntervalNull(y))
                return getNewInterval(-1, -1, false, true);
        long a, b, c, d;
        a = x->min;
        b = x->max;
        c = y->min;
        d = y->max;
	long min, max;
        if (isBoundedByMinusInfinity(a,c)){
                min = std::numeric_limits<long>::min();
        } else{
                min = a - c;
        }

        if (isBoundedByInfinity(b,d)){
                max = std::numeric_limits<long>::max();
        } else {
                max = b - d;
        }
	return getNewInterval(min, max, false, false);
}

Interval* OpUDiv(Interval* x, Interval* y){
	/* Interval Unsigned Division implementation */
	if (isIntervalNull(x) || isIntervalNull(y))
                return getNewInterval(-1, -1, false, true);
        long a, b, c, d;
        a = x->min;
        b = x->max;
        c = y->min;
        d = y->max;
	long min, max;
	if (c==0 && d == 0) {
		return getNewInterval(-1,-1, false, true);		
	}

	if (c>=0){
		min = std::min(std::min(std::min(div(a,c), div(a,d)), div(b,c)), div(b,d));
		max = std::max(std::min(std::min(div(a,c), div(a,d)), div(b,c)), div(b,d));
		return getNewInterval(min, max, false, false);
	}

	if (d<=0){
		return 	OpUDiv(getNewInterval(-b, -a, false, false), getNewInterval(-d, -c, false, false));
	} else {
		return intervalUnion(OpUDiv(getNewInterval(a,b, false, false), getNewInterval(c, 0, false, false)),
				OpUDiv(getNewInterval(a,b, false, false), getNewInterval(0, d, false, false))); 
	}
}

Interval* OpSDiv(Interval* x, Interval* y){
	/* Interval Signed Division implementation */
	if (isIntervalNull(x) || isIntervalNull(y))
                return getNewInterval(-1, -1, false, true);
        long a, b, c, d;
        a = x->min;
        b = x->max;
        c = y->min;
        d = y->max;
        long min, max;
        if (c==0 && d == 0) {
                return getNewInterval(-1,-1, false, true);              
        }

        if (c>=0){
                min = std::min(std::min(std::min(a/c, a/d), b/c), b/d);
                max = std::max(std::min(std::min(a/c, a/d), b/c), b/d);
                return getNewInterval(min, max, false, false);
        }

        if (d<=0){
                return  OpSDiv(getNewInterval(-b, -a, false, false), getNewInterval(-d, -c, false, false));
        } else {
                return intervalUnion(OpSDiv(getNewInterval(a,b, false, false), getNewInterval(c, 0, false, false)),
                                OpSDiv(getNewInterval(a,b, false, false), getNewInterval(0, d, false, false)));
        }

}

long mul(long a, long b){
	/* custom multiplication to handle corner cases */
	if (b == 0 || a == 0){
		return 0;
	} else if (std::numeric_limits<long>::min() == a && b < 0){
		return std::numeric_limits<long>::max();
	} else if (std::numeric_limits<long>::min() == b && a < 0){
		return std::numeric_limits<long>::max();
	} else if (std::numeric_limits<long>::max() == a || std::numeric_limits<long>::max() == b){
		return std::numeric_limits<long>::max();
	} 
	return a*b;
}

long div(long a, long b){
	/* Custom division to handle corner cases */
	if (a == 0 && b == 0){
		return 0;
	} else if (b == 0 && a < 0){
		return std::numeric_limits<long>::min();
	} else if (b == 0 && a > 0){
		return std::numeric_limits<long>::max();
	} else if (std::numeric_limits<long>::min() == b || std::numeric_limits<long>::max() == b){
		return 0;
	}
	return a/b;
}

long add(long a, long b){
	/* Custom addition to handle corner cases */
	if(isBoundedByMinusInfinity(a, b))
		return std::numeric_limits<long>::min();
	else if (isBoundedByInfinity(a, b))
		return std::numeric_limits<long>::max();
	else
		return a + b;
}

long sub(long a, long b){
	/* Custom substraction to handle corner cases */
	if(isBoundedByMinusInfinity(a, b))
                return std::numeric_limits<long>::min();
        else if (isBoundedByInfinity(a, b))
                return std::numeric_limits<long>::max();
        else
                return a - b;
}

Interval* intervalUnion(Interval* x, Interval* y){
	/* Returns union of 2 intervals */
	if (isIntervalNull(x))
		return y;
	if (isIntervalNull(y))
		return x;
	long a,b,c,d;
	a = x->min;
	b = x->max;
	c = y->min;
	d = y->max;
	return getNewInterval(std::min(a,c), std::max(b,d), false, false);
}

bool isIntervalNull(Interval* a){
	/* Return true it is a null interval */
	return a->Null;
}

Interval* intervalIntersection(Interval* x, Interval* y){
	/* Returns intersection of 2 intervals */
	if (isIntervalNull(x) || isIntervalNull(y))
		return getNewInterval(-1, -1, false, true);
	long a,b,c,d;
        a = x->min;
        b = x->max;
        c = y->min;
        d = y->max;
	if (std::max(a,c) <= std::min(b,d)){
		return getNewInterval(std::max(a,c), std::min(b,d), false, false);
	} else{
		return getNewInterval(-1, -1, false, true);
	}
}


bool isBoundedByMinusInfinity(long a,long b){
	/* Checks whether one of a or b is bounded by minus infinity */
	return (a == std::numeric_limits<long>::min() || b == std::numeric_limits<long>::min());
}

bool isBoundedByInfinity(long a,long b){
	/* Checks whether one of a or b is bounded by infinity */
        return (a == std::numeric_limits<long>::max() || b == std::numeric_limits<long>::max());
}

Interval* getIntervalForExpression(std::string opCode, Interval* absVal1, Interval* absVal2){
	/* Executes the expression and returns the abstracted interval */
	Interval* resultInterval = new Interval;
	if (opCode.compare("mul") == 0){
		resultInterval = OpMul(absVal1, absVal2);
	} else if (opCode.compare("add") == 0){
		resultInterval = OpAdd(absVal1, absVal2);
	} else if (opCode.compare("sub") == 0){
		resultInterval = OpSub(absVal1, absVal2);
	} else if (opCode.compare("udiv") == 0){
		resultInterval = OpUDiv(absVal1, absVal2);
	} else if (opCode.compare("sdiv") == 0){
		resultInterval = OpSDiv(absVal1, absVal2);
	}  else {
	}
	return resultInterval;
}

Interval* getNewInterval(long min, long max, bool isInfinity, bool isNull){
	/* Function returns a new Interval from (min, max, isInfinity, isNull) */
	Interval* interval = new Interval;
	if (isInfinity){
		min = std::numeric_limits<long>::min();
                max = std::numeric_limits<long>::max();
	}
	interval->min = min;
	interval->max = max;
	
	if (isNull){
		interval->Null = true;
	} else {
		interval->Null = false;
	}
	return interval;
}

std::map<Instruction*, Interval*> createNewMap(std::map<Instruction*, Interval*> oldMap){
	/* Function duplicates a Map */
	std::map<Instruction*, Interval*> newMap;
	for(auto it = oldMap.cbegin(); it != oldMap.cend(); ++it){
		Interval* interval = new Interval;
		interval->min = it->second->min;
		interval->max = it->second->max;
		interval->Null = it->second->Null;
		newMap.insert(std::make_pair(it->first, interval));
        }
	return newMap;
}

std::map<Instruction*, Interval*> mergeMaps(std::map<Instruction*, Interval*> newUpdatedMap, std::map<Instruction*, Interval*> lastBlockInterval){
	/* Function merges 2 maps into one */
	std::map<Instruction*, Interval*> newMap;
	for(auto it = lastBlockInterval.cbegin(); it != lastBlockInterval.cend(); ++it){
                Interval* interval = new Interval;
                interval->min = it->second->min;
                interval->max = it->second->max;
		interval->Null = it->second->Null;
                newMap.insert(std::make_pair(it->first, interval));
        }
	for(auto it = newUpdatedMap.cbegin(); it != newUpdatedMap.cend(); ++it){
                Interval* interval = new Interval;
                interval->min = it->second->min;
                interval->max = it->second->max;
		interval->Null = it->second->Null;
		if (newMap.find(it->first) != newMap.end()){
			newMap[it->first] = intervalUnion(interval, newMap.find(it->first)->second);
		} else {
                	newMap.insert(std::make_pair(it->first, interval));
		}
        }
	return newMap;
}

void outputAbstractedIntervals(std::map<Instruction*, Interval*> bbToIntervalMap){
        for(auto it = bbToIntervalMap.cbegin(); it != bbToIntervalMap.cend(); ++it){
		if (it->first->hasName()){
                	std::string name = it->first->getName().str();
                	Interval *interval = it->second;
			std::string min, max;
			printf("name: %s, Interval: ", name.c_str());
			printf("[");
			if (interval->min <= std::numeric_limits<long>::min())
				printf("-%s"," \u221E");
			else
				printf("%ld", interval->min);
			printf(",");
			if (interval->max >= std::numeric_limits<long>::max())
                                printf("%s"," \u221E");
                        else
                                printf("%ld", interval->max);
                        printf("]\n");
        	}
	}
}

void printIntervals(Interval* trueInterval, Interval* falseInterval){
	printf("True Interval: [%ld, %ld], %d", trueInterval->min, trueInterval->max, trueInterval->Null);
	printf("False Interval: [%ld, %ld], %d", falseInterval->min, falseInterval->max, falseInterval->Null);
}


void outputExpressionGraph(expressionNode* expressionRoot){
	if (expressionRoot){
		printf("\n%s\n", expressionRoot->variable_name.c_str());
		outputExpressionGraph(expressionRoot->left);
		outputExpressionGraph(expressionRoot->right);
	}	
}

void  outputXYPair(XYPair* newPair){
	Interval* intervalX = newPair->X;
	Interval* intervalY = newPair->Y;
}
